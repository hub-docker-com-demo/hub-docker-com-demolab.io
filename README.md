# [hub-docker-com-demo](https://hub-docker-com-demo.gitlab.io/)

Unofficial demo, howto and wiki for Docker images

## Documentation
* [*Docker Tutorial Series*](https://rominirani.com/docker-tutorial-series-a7e6ff90a023)
* [*Dockerfile reference*](https://docs.docker.com/engine/reference/builder/)
* [*Best practices for writing Dockerfiles*](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/)

## Using Docker images
### GitLab
* [*Using Docker images*](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#define-image-and-services-from-gitlab-ci-yml)
* [dockerfile site:gitlab.com](https://www.google.com/search?q=dockerfile+site%3Agitlab.com)

### Travis (with GitHub)
* [*Using Docker in Builds*](https://docs.travis-ci.com/user/docker/)

<div style="text-align: right"><a href="https://gitlab.com/hub-docker-com-demo/hub-docker-com-demo.gitlab.io/edit/master/README.md">Improve this page.</a></div>

